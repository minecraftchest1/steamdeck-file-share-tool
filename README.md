# File Sharing Tool
## A tool to simplify sharing files using ssh. Originally deisgned for the SteamDeck after watching
https://www.youtube.com/watch?v=pvEZELnbPoI.

This tool simplifies the following tasks:
 * (Re)setting your user account password
 * Starting and enabling the ssh server
 * Finding connection info
 * Finding out to connect.

Support for other file sharing utilities (such as Syncthing) may be added in the future.

## Installation

### Archlinux (Steamdeck) and Manjaro

The attacked *.pkg.tar.zst (Pacman package file) installs the script into your path, and provides a menu entry
for your convience, as well as installing needed dependency. Details on how to install to be written.

### Generic

The script in [bin/file-sharing-tool](bin/file-sharing-tool) is a standalone script that can be run directly,
assuming sshd (part of the [openssh](https://archlinux.org/packages/?name=openssh) package on archlinux and
derivatives) and ([zenity](https://archlinux.org/packages/?name=zenity)) are installed. This script should
work on all distros.
 
