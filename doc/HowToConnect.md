# How To Connect

To connect to your device using SSH (which can be started using this tool), you will need an application that supports either SCP and/or SFTP. On Windows, WinSCP can be used. On Linux, Dolphin (the default file manager for KDE Plasma, use by the Steamdeck) supports SFTP. I can not give any advice for MacOS users.

Todo: Add pictures and instructions on how to connect.
