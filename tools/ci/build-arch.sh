#!/bin/bash -x
echo 'Section 1'
echo '---------'
echo ''
tmpdir=`mktemp -d`
export SECURE_FILES_DOWNLOAD_PATH="${tmpdir}/secure"
export HOME=${tmpdir}/home
mkdir ${HOME}
chmod ug+rw -R ${HOME}
mkdir ${SECURE_FILES_DOWNLOAD_PATH}
chmod ug+rw -R ${SECURE_FILES_DOWNLOAD_PATH}

echo 'Section 2'
echo '---------'
echo ''
# Download Secure Files from Gitlab
# https://docs.gitlab.com/ee/ci/secure_files/
#curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
#ls $tmpdir --recursive

echo 'Section 3'
echo '---------'
echo ''
git clone https://aur.archlinux.org/file-sharing-tool.git ${tmpdir}/pkg
cd ${tmpdir}/pkg

echo 'Section 4'
echo '---------'
echo ''
makepkg --nodeps

echo 'Section 5'
echo '---------'
echo ''
#Sign package
#export GNUPGHOME="${tmpdir}/gpg-home"
#mkdir ${GNUPGHOME}
#chmod ug+rw,o-rwx -R ${GNUPGHOME}
#cat ${SECURE_FILES_DOWNLOAD_PATH}/gpg-password | gpg --batch --pinentry-mode loopback --passphrase-fd 0 --import ${SECURE_FILES_DOWNLOAD_PATH}/signing-key.gpg
#cat ${SECURE_FILES_DOWNLOAD_PATH}/gpg-password | gpg --batch --detach-sign --pinentry-mode loopback --passphrase-fd 0 \
#    -u 0x4468E5589C9A50FF --sign file-sharing-tool-*.pkg.tar.zst

echo 'Section 6'
echo '---------'
echo ''
cd ${tmpdir}/pkg
pkg=`ls file-sharing-tool-*-any.pkg.tar.zst`
#pkg-sig=`ls /pkg/file-sharing-tool-*-any.pkg.tar.zst.sig`

echo 'Section 7'
echo '---------'
echo ''
echo ${pkg}
#echo ${pkg-sig}

echo 'Section 8'
echo '---------'
echo ''
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${tmpdir}/pkg/${pkg} \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/file-sharing-tool/${CI_COMMIT_TAG}/${pkg}"
#curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${tmpdir}/pkg/${pkg-sig} \ 
#    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/file-sharing-tool/${CI_COMMIT_TAG}/${pkg-sig}"